# GREET API TASK
A mobile-responsive single-page React application that provides a list of celebrities, that you can filter by category and order by name and price.

## Technologies used
- Javascript
- React
- HTML
- CSS
- MUI
## Installation

**1.** Use **`git clone`** or download this repository.

**2.** Run **`npm install`**.

**3.** Run **`npm start`**.

## Visuals
- Register
![image](greet-api-task/src/assets/readmeImage/home.png)
