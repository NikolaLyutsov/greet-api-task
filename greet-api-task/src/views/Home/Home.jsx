import { useEffect, useState } from 'react';
import Navigation from '../../components/Navigation/Navigation';
import Card from '../../components/Card/Card';
import './Home.css';

const Home = () => {
  const [cards, setCards] = useState([]);
  const [page, setPage] = useState(1);
  const [isLoading, setIsLoading] = useState(false);
  const [selectedCategories, setSelectedCategories] = useState('');
  const [selectedNameOrPrice, setSelectedNameOrPrice] = useState('');

  const extractBulgarianText = (inputString) => {
    const cyrillicRegex = /[а-яА-Я]/g;
      const bulgarianText = inputString.match(cyrillicRegex);
      if (bulgarianText) {
        return bulgarianText.join('');
      } else {
        return 'a';
      }
  }

  const mapByCategories = [...new Set(cards.map((p) => p.categories.map(el => selectedCategories.includes(el.name) ? p : null)).flatMap(el => el.filter(w => w !== null)))];
  const collator = new Intl.Collator('bg');
  const mapByName = cards.map((c) => ({c, bulgarianName: extractBulgarianText(c.name)})).sort((a, b) => collator.compare(a.bulgarianName, b.bulgarianName)).map((el) => el.c);
  const mapByPrice = cards.map((c) => ({c, price: c.prices.price})).sort((a, b) => a.price - b.price).map((el) => el.c);
  const mapByNameAndCategories = mapByCategories.map((c) => ({c, bulgarianName: extractBulgarianText(c.name)})).sort((a, b) => collator.compare(a.bulgarianName, b.bulgarianName)).map((el) => el.c);
  const mapByPriceAndCategories = mapByCategories.map((c) => ({c, price: c.prices.price})).sort((a, b) => a.price - b.price).map((el) => el.c);
    
  useEffect(() => {
    let renderOnce = false;
      setIsLoading(true);
      fetch(`https://greet.bg/wp-json/wc/store/products?page=${page}`)
      .then((response) => {
        return response.json();
      })
      .then((result) => {
        if(!renderOnce) {
        setCards(prevCards => [...prevCards, ...result]);
        }
      })
      .catch((error) => {
        console.log(error.message);
      })
      .finally(() => {
        setIsLoading(false);
      })
      return () => {
        setPage(page + 1);
        renderOnce = true;
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    const handleScroll = () => {
      if (window.innerHeight + document.documentElement.scrollTop !== document.documentElement.offsetHeight || isLoading) {
          return;
      }
      setIsLoading(true);
      fetch(`https://greet.bg/wp-json/wc/store/products?page=${page}`
      )
      .then((response) => {
          return response.json();
      })
      .then((result) => {
          setPage(prevPage => prevPage + 1);
          setCards(prevCards => [...prevCards, ...result]);
      })
      .catch((error) => {
          console.log(error.message);
      })
      .finally(() => {
          setIsLoading(false);
      })
    };
    window.addEventListener('scroll', handleScroll);
    return () => window.removeEventListener('scroll', handleScroll);
  }, [isLoading, page]);

  const handleCategoriesData = (data) => {
    setSelectedCategories(data);
  };
  const handleNameOrPriceData = (data) => {
    setSelectedNameOrPrice(data);
  };

  return (
    <div className='home'>
      <Navigation onHandleCategories={handleCategoriesData} onHandleNameOrPriceChange={handleNameOrPriceData}/>
      {
      selectedCategories && selectedNameOrPrice === 'name' ?
      mapByNameAndCategories.map((c) => <Card key={c.id} name={c.name} image={c.images[0].src} price={c.prices.price} 
      currency={c.prices.currency_code} description={c.short_description} categories={c.categories}/>)
      :
      (selectedCategories && selectedNameOrPrice === 'price' ?
      mapByPriceAndCategories.map((c) => <Card key={c.id} name={c.name} image={c.images[0].src} price={c.prices.price} 
      currency={c.prices.currency_code} description={c.short_description} categories={c.categories}/>)
      :
      (selectedCategories ? 
      mapByCategories.map((c) => <Card key={c.id} name={c.name} image={c.images[0].src} price={c.prices.price} 
      currency={c.prices.currency_code} description={c.short_description} categories={c.categories}/>)
      :
      (selectedNameOrPrice === 'name' ?
      mapByName.map((c) => <Card key={c.id} name={c.name} image={c.images[0].src} price={c.prices.price} 
      currency={c.prices.currency_code} description={c.short_description} categories={c.categories}/>)
      :
      (selectedNameOrPrice === 'price' ?
      mapByPrice.map((c) => <Card key={c.id} name={c.name} image={c.images[0].src} price={c.prices.price} 
      currency={c.prices.currency_code} description={c.short_description} categories={c.categories}/>)
      :
      cards.map((c) => <Card key={c.id} name={c.name} image={c.images[0].src} price={c.prices.price} 
      currency={c.prices.currency_code} description={c.short_description} categories={c.categories}/>)
      ))))
    }
      {isLoading && <h1>Loading...</h1>}
    </div>
  );
};
export default Home;
