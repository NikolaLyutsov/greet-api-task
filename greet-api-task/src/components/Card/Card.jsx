import AddToCartBtn from '../AddToCartBtn/AddToCartBtn';
import './Card.css';

const Card = ({name, image, description, categories, price, currency}) => {
    const decodeHtmlEntities = (input) => {
      const doc = new DOMParser().parseFromString(input, 'text/html');
      return doc.documentElement.textContent;
    }
    return (
      <div className='card'>
        <h1>{decodeHtmlEntities(name)}</h1>
        <img src={image} alt="profile" className='profileImage'/>
        <h3>{decodeHtmlEntities(description)}</h3>
        <h3>Categories: {categories.map((cat, i) => <span key={cat.id}>{(i ? ', ' : '') + cat.name}</span>)}</h3>
        <h3>Price: {price} {currency}</h3>
        <AddToCartBtn/> 
      </div>
    )
}
export default Card;