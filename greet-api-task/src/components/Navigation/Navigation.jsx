import { useState } from "react";
import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import OutlinedInput from "@mui/material/OutlinedInput";
import InputLabel from "@mui/material/InputLabel";
import FormControl from "@mui/material/FormControl";
import ListItemText from "@mui/material/ListItemText";
import Checkbox from "@mui/material/Checkbox";
import { Select, MenuItem } from "@mui/material";
import {ALL_CATEGORIES, ITEM_HEIGHT, ITEM_PADDING_TOP} from '../../common/constants';
import "./Navigation.css";

export default function Navigation(props) {
  const [categories, setCategories] = useState([]);
  const [nameOrPrice, setNameOrPrice] = useState("");

  const MenuProps = {
    PaperProps: {
      style: {
        maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
        width: 250,
      },
    },
  };

  const handleNameOrPriceChange = (event) => {
    const secondValue = event.target.value;
    setNameOrPrice(secondValue);
    props.onHandleNameOrPriceChange(secondValue);
  };

  const handleCategories = (event) => {
    const firstValue = event.target.value;
    setCategories(firstValue);
    props.onHandleCategories(firstValue);
  };
  return (
    <Box sx={{ flexGrow: 1 }} id="navigationContainer">
      <AppBar position="fixed" style={{ background: "#3c2a73" }}>
        <Toolbar>
          <div>
            <FormControl sx={{ m: 2 }} id="selectCategories">
              <InputLabel id="demo-multiple-checkbox-label">
                Categories
              </InputLabel>
              <Select
                labelId="demo-multiple-checkbox-label"
                id="demo-multiple-checkbox"
                multiple
                value={categories}
                onChange={handleCategories}
                input={<OutlinedInput label="Categories" />}
                renderValue={(selected) => selected.join(", ")}
                MenuProps={MenuProps}
                sx={{ color: "white" }}
              >
                {ALL_CATEGORIES.map((name) => (
                  <MenuItem key={name} value={name}>
                    <Checkbox checked={categories.indexOf(name) > -1} />
                    <ListItemText primary={name} />
                  </MenuItem>
                ))}
              </Select>
            </FormControl>
            <FormControl sx={{ m: 2 }} id="selectNameOrPrice">
              <InputLabel id="demo-simple-select-label">
                Name or Price
              </InputLabel>
              <Select
                labelId="demo-simple-select-label"
                id="demo-simple-select"
                value={nameOrPrice}
                label="NameOrPrice"
                onChange={handleNameOrPriceChange}
                sx={{ color: "white" }}
              >
                <MenuItem value="name">Name</MenuItem>
                <MenuItem value="price">Price</MenuItem>
              </Select>
            </FormControl>
          </div>
        </Toolbar>
      </AppBar>
    </Box>
  );
}
